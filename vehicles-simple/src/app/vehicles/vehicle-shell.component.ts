import { Component } from '@angular/core';
import { VehicleListComponent } from "./vehicle-list.component";
import { VehicleDetailComponent } from "./vehicle-detail.component";
import { CartComponent } from '../cart/cart.component';

@Component({
  selector: 'his-vehicle-shell',
  standalone: true,
  template: `
  <div class='row'>
    <div class='col-md-3'>
        <his-vehicle-list></his-vehicle-list>
    </div>
    <div class='col-md-9'>
      <div class='row'>
         <his-vehicle-detail></his-vehicle-detail>
      </div>
      <div class='row'>
         <his-cart></his-cart>
      </div>
    </div>
</div>
  `,
  imports: [VehicleListComponent, VehicleDetailComponent, CartComponent]
})
export class VehicleShellComponent {

}
