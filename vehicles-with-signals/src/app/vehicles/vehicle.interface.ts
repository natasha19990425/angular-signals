export interface Vehicle {
  cargo_capacity: number;
  crew: number;
  name: string;
  model: string;
  manufacturer: string,
  costCredits: string
  passengers: number;
  vehicleClass: string;
  films: string[];
  price?: number;
}

export interface Film {
  title: string;
}

export interface VehicleResponse {
    count: number;
    next: string;
    previous: string;
    results: Vehicle[]
  }
  