import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, inject, signal } from '@angular/core';
import {
  catchError,
  filter,
  forkJoin,
  map,
  Observable,
  shareReplay,
  switchMap,
  throwError
} from 'rxjs';
import { toSignal, toObservable } from '@angular/core/rxjs-interop';
import { Vehicle ,VehicleResponse ,Film} from './vehicle.interface';
 

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  http = inject(HttpClient);

  private url = 'https://swapi.py4e.com/api/vehicles';

  // 車輛第一頁
  // 如果價格為空，則隨機分配一個價格
  //（我們不能在這個演示中修改後端）
  private vehicles$ = this.http.get<VehicleResponse>(this.url).pipe(
    map((data) =>
      data.results.map((x) => ({
        ...x,
        costCredits: isNaN(Number(x.costCredits)) ? String(Math.random() * 100000) : x.costCredits,
      }) as Vehicle)
    ),
    shareReplay(1),
    catchError(this.handleError)
  );

  // 公開來自該服務的信號
  vehicles = toSignal<Vehicle[], Vehicle[]>(this.vehicles$, { initialValue: [] });
  selectedVehicle = signal<Vehicle | undefined>(undefined);

  private vehicleFilms$ = toObservable(this.selectedVehicle).pipe(
    filter(Boolean),
    switchMap(vehicle =>
      forkJoin(vehicle.films.map(link =>
        this.http.get<Film>(link)))
    )
  );

  vehicleFilms = toSignal<Film[], Film[]>(this.vehicleFilms$, { initialValue: [] });

  vehicleSelected(vehicleName: string) {
    const foundVehicle = this.vehicles().find((v) => v.name === vehicleName);
    this.selectedVehicle.set(foundVehicle);
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    // 在真實世界的應用程序中，我們可能會將服務器發送到一些遠程日誌記錄基礎設施
    // 而不是僅僅將其記錄到控制台
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // 發生客戶端或網絡錯誤。 相應地處理它。
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // 後端返回一個不成功的響應碼。
      // 響應正文可能包含出錯的線索，
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message
        }`;
    }
    console.error(errorMessage);
    return throwError(() => errorMessage);
  }
}
