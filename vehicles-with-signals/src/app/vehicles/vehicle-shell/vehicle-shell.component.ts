import { Component } from '@angular/core';
import { VehicleListComponent } from "../vehicle-list/vehicle-list.component";
import { VehicleDetailComponent } from "../vehicle-detail/vehicle-detail.component";

@Component({
  selector: 'his-vehicle-shell',
  standalone: true,
  templateUrl: 'vehicle-shell.component.html',
  imports: [VehicleListComponent, VehicleDetailComponent]
})
export class VehicleShellComponent {

}
