import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UserListComponent } from '../user-list/user-list.component';
import { UserTodosComponent } from "../../todos/user-todos/user-todos.component";

@Component({
  selector: 'app-user-shell',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './user-shell.component.html',
  imports: [UserListComponent, UserTodosComponent]
})
export class UserShellComponent {

}
